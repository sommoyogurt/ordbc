package debug;

import java.util.*;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;


import com.tomting.orion.*;
import com.tomting.orion.connection.OrionConnection;
import com.tomting.orion.ordbc.Ordbc;

public class DebugThrift {

	/**
	 * @param args
	 * @throws TException 
	 */
	public static void main(String[] args) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		
		service.iVservicetype = iEservicetype.STATEMENT;
		service.cVstatement = OrionConnection.getStatement();
		
		ThrfLmtb mutable = service.cVstatement.cVmutable;
		mutable.sVtable = "NEWAUTOGENERATE";
		mutable.sVnamespace = "DEFAULT";		
		
		ThrfLkey key = service.cVstatement.cVkey;
		key.sVmain = "KEY3";
		key.iVstate = iEstatetype.UPSERT;
		key.iVtimestamp = 200000000000l;

		List<ThrfL2cl> columns = service.cVstatement.cVcolumns;
		ThrfL2cl column = new ThrfL2cl ();
		column.sVcolumn = "COLUMN";
		column.cVvalue = new ThrfL2cv ();
		ThrfL2cv value = column.cVvalue;
		value.sVvalue = "VALUE2";
		value.iVtype = iEcolumntype.STRINGTYPE;
		columns.add(column);
		
		TSerializer serializer = new TSerializer ();
		byte [] buf = serializer.serialize (service);

				
		Ordbc ordbc = new Ordbc ("IPC", 9000, 8, 50);
		byte [] answer = ordbc.runService(buf, false);
		ordbc.close();	
		
		if (answer != null) {
			ThrfSrvr serviceResult = new ThrfSrvr ();		
			TDeserializer deserializer = new TDeserializer ();
			deserializer.deserialize(serviceResult, answer);
			System.out.println ("risultato " + serviceResult.bVreturn);
		}
		
		
		
	}
	
	static {
		System.loadLibrary(Ordbc.ORDBC); 
	}	

}
