package debug;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.thrift.TException;

import com.tomting.orion.ThrfL2cl;
import com.tomting.orion.ThrfL2cv;
import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.ThrfL2qb;
import com.tomting.orion.ThrfL2qr;
import com.tomting.orion.ThrfL2st;
import com.tomting.orion.ThrfLkey;
import com.tomting.orion.iEcolumntype;
import com.tomting.orion.iEquerytype;
import com.tomting.orion.iEstatetype;
import com.tomting.orion.connection.Helpers;
import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnection;

public class DebugLaunch {

    /**
     * string to serializable
     */
	public static Object fromString( String s ) throws IOException ,
                                                        ClassNotFoundException {
        byte [] data = Base64Coder.decode( s );
        ObjectInputStream ois = new ObjectInputStream( 
                                        new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    /**
     * serializable to string
     */
    public static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return new String( Base64Coder.encode( baos.toByteArray() ) );
    }		
	
	
	/**
	 * @param args
	 * @throws TException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws TException, IOException, ClassNotFoundException {
		String url = "orion:[7,50]localhost:9001,9000:DEFAULT";
		String table = "MEDUSALONG";
		String value = "VALUE";
		String timestamp = "TIMESTAMP";
		
		OCFI orionConnectionFactory = 
				com.tomting.aries.Helpers.getSharedACF(Helpers.getOCF(url), url);		
		
		ThrfL2st statement = OrionConnection.getStatement();
		statement.cVkey.sVmain = "TEST";
		statement.cVkey.iVstate = iEstatetype.UPSERT;
		statement.cVmutable.sVtable = table;
        
		ThrfL2cl cVcolumn = new ThrfL2cl ();
        cVcolumn.cVvalue = new ThrfL2cv ();
        cVcolumn.cVvalue.iVtype = iEcolumntype.STRINGTYPE;
        cVcolumn.cVvalue.sVvalue = toString ("hello");
        cVcolumn.sVcolumn = value; 
        statement.cVcolumns.add(cVcolumn);  
      
        cVcolumn = new ThrfL2cl ();
        cVcolumn.cVvalue = new ThrfL2cv ();
        cVcolumn.cVvalue.iVtype = iEcolumntype.STRINGTYPE;
        cVcolumn.cVvalue.sVvalue = "111111111111"; 
        cVcolumn.sVcolumn = timestamp; 
        statement.cVcolumns.add(cVcolumn);         
        
        for (int iV = 0; iV < 100000; iV++) {
        	statement.cVkey.sVmain = "TEST" + iV;
	        OCI orionS = orionConnectionFactory.checkOut(); 
	        orionS.runStatement(statement);   
	        //System.out.println ("result " + orionS.runStatement(statement) + iV);        	
			orionConnectionFactory.checkIn(orionS);
        }
		
		ThrfL2qr query = OrionConnection.getQuery();
		query.cVmutable.sVtable = table;
		query.iVquery = iEquerytype.EXACTQUERY;
		query.cVkey_start = new ThrfLkey ();					
		query.cVkey_start.sVmain = "TEST0";	
		
        cVcolumn = new ThrfL2cl ();
        cVcolumn.sVcolumn = value;
        query.cVselect.add(cVcolumn);  		
		

        System.out.println ("inizio");
        for (int iV = 0; iV < 100000; iV++) {        	
	        OCI orion = orionConnectionFactory.checkOut();       
	    	ThrfL2qb queryReturn = orion.runQuery (query);
	    	boolean returnedKeyslices = queryReturn.bVreturn;
	    	try {
		    	if (returnedKeyslices) {
		    		ThrfL2ks keyslice = queryReturn.cKeyslices.get(0);
		    		String opaqueValue = keyslice.cVcolumns.get(0).cVvalue.sVvalue;
		    		System.out.println ("valore " + fromString (opaqueValue) + " " + iV);
		    	}
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	}
			orionConnectionFactory.checkIn(orion);
        }
		
		orionConnectionFactory.close();
		System.out.println ("main end");
	}
}
