package debug;

import com.tomting.orion.ordbc.CustomClassLoader;
import com.tomting.orion.ordbc.ORDBCI;

public class DebugClassloader {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static void main(String[] args) throws Exception{
		CustomClassLoader cl = new CustomClassLoader ();
		Class<?> ordbcClass = cl.findClass("com.tomting.orion.ordbc.Ordbc");
		ORDBCI ordbc = (ORDBCI) ordbcClass.newInstance();
		ordbc.open("IPC", 9000, 8, 50);
		ordbc.close();
		ordbcClass = null;
		ordbc = null;
		cl = null;
		System.gc ();
	}
	
	
}
