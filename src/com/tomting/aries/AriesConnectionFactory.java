package com.tomting.aries;

import org.apache.thrift.TException;

import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnectionFactory;

public class AriesConnectionFactory implements OCFI {
	AriesConnection oc;
	OrionConnectionFactory ocf;
	
	public AriesConnectionFactory (OrionConnectionFactory ocf, int workers, int bulkSize) {
		
		this.ocf = ocf;
		oc = new AriesConnection (ocf, workers, bulkSize);
	}
		
	@Override
	public void close () {
		try {
			oc.close();
		} catch (TException e) {
		}
	}
	
	@Override
	public void checkIn(OCI oc) {

	}

	@Override
	public OCI checkOut() {

		return oc;
	}
	
	@Override	
	public String getHost () {
		
		return ocf.getHost ();
	}

	@Override	
	public int getPortIO () {
		
		return ocf.getPortIO ();
	}

	@Override	
	public int getPortNIO () {
		
		return ocf.getPortNIO ();
	}	

}
