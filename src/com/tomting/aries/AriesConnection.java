package com.tomting.aries;

import java.util.List;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;

import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.ThrfL2os;
import com.tomting.orion.ThrfL2qb;
import com.tomting.orion.ThrfL2qr;
import com.tomting.orion.ThrfL2st;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;
import com.tomting.orion.iEservicetype;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnectionFactory;
import com.tomting.orion.ordbc.CustomClassLoader;
import com.tomting.orion.ordbc.ORDBCI;
import com.tomting.orion.ordbc.Ordbc;

public class AriesConnection  implements OCI {
	String namespace;
	final String LOCALHOST = "localhost";
	CustomClassLoader cl;
	ORDBCI ordbc;
	TSerializer serializer = new TSerializer ();
	TDeserializer deserializer = new TDeserializer ();
	
	public AriesConnection (OrionConnectionFactory ocf, int workers, int bulkSize) {
		
		this.namespace = ocf.getNamespace();
		String host = ocf.getHost();
		host = host.equals(LOCALHOST) ? "IPC" : host;
		/*
		cl = new CustomClassLoader ();
		Class<?> ordbcClass;
		try {
			ordbcClass = cl.findClass("com.tomting.orion.ordbc.Ordbc");
			ordbc = (ORDBCI) ordbcClass.newInstance();
			ordbc.open(host, ocf.getPortIO(), workers, bulkSize);
		} catch (Exception e) {
			ordbc = null;
		}	*/		
		ordbc = new Ordbc (host, ocf.getPortIO(), workers, bulkSize);
	}
	
	@Override
	public void close() throws TException {
		
		if (ordbc != null) {
			ordbc.close();
			ordbc = null;
			cl = null;
			System.gc ();		
		}
	}	
		
	@Override
	public List<ThrfL2ks> runOsql(ThrfL2os osql) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		ThrfSrvr serviceResult = new ThrfSrvr ();
		
		service.cVosql = osql;
		service.cVosql.sVnamespace = namespace;
		service.iVservicetype = iEservicetype.OSQL;
		deserializer.deserialize (serviceResult, 
				ordbc.runService(
						serializer.serialize(service), false));
		return serviceResult.cVdmlresult;
	}

	@Override
	public ThrfL2qb runQuery(ThrfL2qr query) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		ThrfSrvr serviceResult = new ThrfSrvr ();
		ThrfL2qb queryResult = new ThrfL2qb ();
		
		service.cVquery = query;
		service.cVquery.cVmutable.sVnamespace = namespace;		
		service.iVservicetype = iEservicetype.QUERY;
		deserializer.deserialize (serviceResult, 
				ordbc.runService(
						serializer.serialize(service), false));
		queryResult.bVreturn = serviceResult.bVreturn;
		queryResult.cKeyslices = serviceResult.cVdmlresult;
		return queryResult;
	}

	@Override
	public boolean runStatement(ThrfL2st statement) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		ThrfSrvr serviceResult = new ThrfSrvr ();		
		
		service.cVstatement = statement;
		service.cVstatement.cVmutable.sVnamespace = namespace;	
		service.cVstatement.cVkey.iVtimestamp = com.tomting.orion.connection.Helpers.getTimestamp();
		service.iVservicetype = iEservicetype.STATEMENT;
		deserializer.deserialize (serviceResult, 
				ordbc.runService(
						serializer.serialize(service), false));		
		return serviceResult.bVreturn;
	}

	@Override
	public void runStatementFast(ThrfL2st statement) throws TException {
		ThrfSrvc service = new ThrfSrvc ();
		
		service.cVstatement = statement;
		service.cVstatement.cVmutable.sVnamespace = namespace;	
		service.cVstatement.cVkey.iVtimestamp = com.tomting.orion.connection.Helpers.getTimestamp();
		service.iVservicetype = iEservicetype.STATEMENT;
		ordbc.runService(serializer.serialize(service), true);
	}	


	@Override
	public boolean isOpen() {
		
		return ordbc != null;
	}
	
}
