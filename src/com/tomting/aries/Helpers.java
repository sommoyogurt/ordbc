package com.tomting.aries;

import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import org.apache.log4j.Logger;

import com.tomting.orion.connection.OCFI;
import com.tomting.orion.connection.OrionConnectionFactory;


public class Helpers {
	private final static Logger LOGGER = Logger.getLogger(Helpers.class.getName());		
	public static final ConcurrentHashMap<OrionConnectionFactory, OCFI> 
		ariesConnectionFactoryPool = new ConcurrentHashMap<OrionConnectionFactory, OCFI> ();
	
    /**
     * aries OCFI or orion OCFI if workers == 0
     * @param ocf
     * @param url
     * @return
     */
    static public synchronized OCFI getSharedACF (OrionConnectionFactory ocf, String url)  {
    	
    	Matcher hostMatcher = com.tomting.orion.connection.Helpers.URL.matcher(url);
        LOGGER.info(url);   	
		if (!hostMatcher.matches()) 
			throw new UnsupportedOperationException("AriesConnectionFactory: orion:[work,bulkSize]host:port:namespace [" + url + "]");  		
        int workers = hostMatcher.group(3) != null ? Integer.parseInt (hostMatcher.group(3)) : 0;	
        int bulkSize = hostMatcher.group(4) != null ? Integer.parseInt (hostMatcher.group(4)) : 0;	        
        LOGGER.info(url);
        if (workers > 0) {
        	if (ariesConnectionFactoryPool.get(ocf) == null) 
        		ariesConnectionFactoryPool.putIfAbsent(ocf, new AriesConnectionFactory (ocf, workers, bulkSize));
        }
        else ariesConnectionFactoryPool.putIfAbsent(ocf, ocf);
        return ariesConnectionFactoryPool.get(ocf);
    }
	    
	/**
	 * aries OCFI
	 * @param ocf
	 * @param workers
	 * @param bulkSize
	 * @return
	 */
    static public OCFI getSharedACF (	OrionConnectionFactory ocf, int workers, int bulkSize) {
    	
    	ariesConnectionFactoryPool.putIfAbsent(ocf, new AriesConnectionFactory (ocf, workers, bulkSize));
    	return ariesConnectionFactoryPool.get(ocf);
    }	    
	
}
