package com.tomting.orion.ordbc;

public interface ORDBCI {
	
	public void open (String address, int port, int workers, int bulkSize);	
	public void close ();	
	public byte [] runService (byte [] service, boolean optimistic);
}
