package com.tomting.orion.ordbc;

public class Ordbc implements ORDBCI {
	public static String ORDBC = "ordbc";
	boolean alive;
	private native boolean initializeOrdbc (String address, int port, int workers, int bulkSize);
	private native boolean destroyOrdbc ();
	private native byte [] run (byte [] service, boolean optimistic);
	
	public Ordbc () {
		alive = false;
	}
	
	public Ordbc (String address, int port, int workers, int bulkSize) {
		alive = false;
		open (address, port, workers, bulkSize);
	}
	
	/**
	 * 
	 * @param address
	 * @param port
	 * @param workers
	 * @param bulkSize
	 */
	public void open (String address, int port, int workers, int bulkSize) {
		
		if (!alive) {
			initializeOrdbc (address, port, workers, bulkSize);
			alive = true;
		}
	}
	
	public synchronized void close () {
		
		if (alive) {
			destroyOrdbc ();
			alive = false;
		}
	}
	
	@Override
    protected void finalize() throws Throwable {
 
		try {
			close ();
        } catch(Throwable t) {
        	t.printStackTrace();
            throw t;
        } finally{
            super.finalize();
        }
     
    }

	
	/**
	 * 
	 * @param service
	 * @param optimistic
	 * @return
	 */
	public byte [] runService (byte [] service, boolean optimistic) {
		
		return run (service, optimistic);
	}
		
	static {
		System.loadLibrary(ORDBC); 
	}
	
}
